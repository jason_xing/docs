# gitlab
## 1. 创建新项目
1>. 第一步
![](images/Home.jpg)
2>. 第二步
![](images/NewProject1.jpg)
3>. 第三步
![](images/NewProject2.jpg)
> **注意：**  
* You won't be able to pull or push project code via SSH until you add an SSH key to your profile.  
* 但可以通过HTTPS来pull或push，输入用户名、密码即可。  
* 将ssh的公钥配置到要访问的服务器上目的：免密码登录。  
* ssh将公钥保存至gitlab，每次连接时客户端发送本地私钥（默认~/.ssh/id_rsa）到服务器端同公钥进行验证。

4>. 第四步
![](images/NewProject3.jpg)
> **注意：**  
* git config -global 命令会修改用户目录下的.gitconfig文件，只需做一次即可，它是针对每一个ssh用户的。  
* 图文中的两个命令用途：提交代码的log里面会显示提交者的信息，如果不修改，log里面显示的提交者信息是本机.ssh公钥内的UseName@HostName。
